﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using GDotnet.Reader.Api.DAL;
//using GDotnet.Reader.Api.Protocol.Gx;
using UCchip.Reader.API;
using static UCchip.Reader.API.UCchipClient;


namespace API
{
    internal class Program
    {
        static void Main(string[] args)
        {
            byte[] UcDataEpcStart = new byte[1] { 0x00 };

            var status = UCchip.Reader.API.ErrorCode.OK;

            var ports = UCchipClient.GetCOMportList();

            ports[0] = "COM33";
            Console.WriteLine(ports[0]);

            var readerClient = new UCchipClient();

            readerClient.OpenSerial(ports[0] + ":115200", 3000, out status);
            //readerClient.OpenEthernet("192.168.0.163", 5050, out status);

            Console.WriteLine("open result:" + status);

            ///////////////////////////////////////////////////////////////
            /////20220615 -- 注册接收数据回调接口
            readerClient.SerialPortLog += WritePortLog;
            readerClient.CommandResponceReceived += CommandResponceReceived;

            //readerClient.SendCommand(Commands.cmd_get_firmware_version);

            ////盘存demmo
            ////复位
            status  = readerClient.MsgBaseReset();
            if (status == UCchip.Reader.API.ErrorCode.OK)
            {
                Console.WriteLine("reset success.");
            }
            else
            {
                Console.WriteLine("reset failed.");
            }
            System.Threading.Thread.Sleep(500);//ms
            ////开始盘存
            //readerClient.MsgStartInventory(1);
            //System.Threading.Thread.Sleep(10 * 1000);//ms
            ////结束盘存
            //readerClient.MsgBaseStopInventory();


            //复位读写器
            //readerClient.MsgBaseReset();
            //设置波特率  --  115200
            //readerClient.MsgBaseSetUartBoudrate(9);
            ////获取固件版本
            //status = readerClient.MsgBaseGetFirmwareVer();
            //if (status == UCchip.Reader.API.ErrorCode.OK)
            //{
            //    Console.WriteLine("get firmware version success.");
            //}
            //else
            //{
            //    Console.WriteLine("get firmware version failed.");
            //}
            ////设置读卡器地址
            //readerClient.MsgBaseSetReaderAddress(0x08);
            ////设置工作天线
            //readerClient.MsgBaseSetWorkAntenna(1);
            ////获取工作天线
            //readerClient.MsgBaseGetWorkAntenna();
            ////设置发射功率
            //readerClient.MsgBaseSetOutputPower(30);
            ////获取发射功率
            //readerClient.MsgBaseGetOutputPower();
            ////设置频率范围 -- 系统定义
            //readerClient.MsgBaseSetFreqRegion(1, 0x07, 0x3b);
            ////设置频率范围 -- 用户自定义
            //readerClient.MsgBaseSetFreqRegion(250, 2, 915000);
            ////获取工作频点
            //readerClient.MsgBaseGetFreqRegion();
            ////获取读卡器温度
            //readerClient.MsgBaseGetReaderTemp();
            ////设置临时的发送功率
            //readerClient.MsgBaseSetTemporaryOutputPower(30);
            ////设置链接
            //readerClient.MsgBaseSetRfLinkProfile(0xd6);
            ////获取链接
            //readerClient.MsgBaseGetRfLinkProfile();
            ////盘存，缓存模式
            //readerClient.MsgBaseInventory(1);
            ////读
            //readerClient.MsgBaseRead(0x00, 1, 1, 0x00000000);
            ////写
            //readerClient.MsgBaseWrite(0x00000000, 0x01, 2, 1, new byte[2] { 0x01,0x02});
            ////锁
            //readerClient.MsgBaseLock(0x00000000, 0x03, 0x00);
            ////kill
            //readerClient.MsgBaseKill(0x12345678);
            ////设置匹配接入的epc
            //readerClient.MsgBaseSetAccessEpcMatch(0x00, 1, new byte[1] { 0x00 });
            ////获取匹配接入的epc
            //readerClient.MsgBaseGetAccessEpcMatch();
            ////自定义盘存
            //readerClient.MsgBaseCustmizedSessionTargetInventoty(0x00, 0x00);
            ////读卡器参数重置
            //readerClient.MsgBaseReaderParaReset();
            ////读卡器应用升级
            //readerClient.MsgBaseReaderAppUpgrade(0, 0x00, new byte[200] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14 });
            ////基带固件升级
            //readerClient.MsgBasebandFirmwareUpgrade(1, 0x00, new byte[20] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14 });


            ////升级应用demo -- 阻塞式方法
            //readerClient.MsgBaseAppUpgrage(@"E:\YXwork\16_RFID\API\8088.bin", UpdataCallback);
            //升级基带软件demo -- 阻塞式方法
            //readerClient.MsgBasebandUpgrade(@"E:\YXwork\16_RFID\READER\BIN\8288\8288.bin", UpdataCallback);
            //Console.WriteLine("升级结束...\r\n");



            System.Threading.Thread.Sleep(10000);//ms
            Console.WriteLine("断开连接...\r\n");

            //关闭通道
            readerClient.CloseSerial(out status);
            //readerClient.CloseNetwork(out status);
            Console.ReadLine();
        }

        private static void WritePortLog(string message)
        {
            Console.WriteLine(message);
        }

        //20220615
        private static void CommandResponceReceived(byte[] buffer)
        {
            Console.WriteLine("recevied:");
            Console.WriteLine(ToHexString(buffer));
        }

        private static void UpdataCallback(long DataLength, long DataCount)
        {
            Console.WriteLine("DataLength:" + DataLength);
            Console.WriteLine("DataCount:" + DataCount);
        }
    }
}
