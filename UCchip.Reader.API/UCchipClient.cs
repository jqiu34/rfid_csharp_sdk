﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Net;
using System.Net.Sockets;

namespace UCchip.Reader.API
{
    public class UCchipClient : IDisposable
    {
        #region message output

        /// <summary>
        /// 订阅com信息，包括输入和输出
        /// </summary>
        /// <param name="message"></param>
        public delegate void DelegateSerialPortLog(string message);
        public DelegateSerialPortLog SerialPortLog;

        /// <summary>
        /// 分割的，一个一个命令的返回的buffer
        /// </summary>
        /// <param name="buffer"></param>
        public delegate void DelegateSerialPortReceivedResponce(byte[] buffer);
        public DelegateSerialPortReceivedResponce CommandResponceReceived;

        // 委托，升级下位机软件返回状态等信息
        public delegate void GetUpgrageCallback(long DataLength, long DataCount);

        private void WriteLog(string message)
        {
            if (SerialPortLog != null)
                SerialPortLog(message);
        }

        // 开一个定时器
        System.Timers.Timer Upgradetimer = new System.Timers.Timer(5000);

        private void SendOutCommandResponce(byte[] cmdResponce)
        {
            if (CommandResponceReceived != null)
                CommandResponceReceived(cmdResponce);

            int bufLen = cmdResponce.Length;
            if(bufLen < 5)
            {
                return;
            }
            int dataLen = bufLen - 5;
            int index = 0;

            var commandContent = new CommandContent();
            index++;                                                    //head
            index++;                                                    //len
            commandContent.Address = cmdResponce[index++];              //addr
            commandContent.Command = cmdResponce[index++];              //cmd
            if(dataLen > 0)                                             //data
            {
                commandContent.Data = cmdResponce.Skip(index).Take(dataLen).ToArray();
            }
            index += dataLen;
            index++;                                                    //crc

            ResponceUnpacket(commandContent.Command, commandContent.Data);

            Console.WriteLine("index:" + index);
        }

        private void ResponceUnpacket(byte cmd, byte[] data)
        {
            uint index = 0;
            int packetnum = 0;
            byte temp = 0;

            Console.WriteLine("解析数据：" + ToHexString(data));

            if(ReponseCommand == (int)cmd)
            {
                ResponseFlag = true;
                Upgradetimer.Stop();
                Console.WriteLine("收到应答。");
            }

            switch (cmd)
            {
                case (byte)(Commands.cmd_reader_app_upgrade):
                    temp = data[index++];
                    packetnum = temp * 256 * 256 * 256;
                    temp = data[index++];
                    packetnum += temp * 256 * 256;
                    temp = data[index++];
                    packetnum += temp * 256;
                    temp = data[index++];
                    packetnum += temp;

                    temp = data[index++];
                    if(packetnum == parameter.packetcount)
                    {
                        parameter.gradestatus = 0x01;
                        if (temp == 0x00)
                        {
                            Console.WriteLine("升级接收的指令正确 -- 8088.");
                        }
                        else if (temp == 0x01)
                        {
                            Console.WriteLine("升级结束.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("错误应答 -- 8088.");
                        parameter.gradestatus = 0x02;
                    }
                    break;
                case (byte)(Commands.cmd_baseband_firmware_upgrade):
                    temp = data[index++];
                    packetnum = temp * 256 * 256 * 256;
                    temp = data[index++];
                    packetnum += temp * 256 * 256;
                    temp = data[index++];
                    packetnum += temp * 256;
                    temp = data[index++];
                    packetnum += temp;

                    temp = data[index++];
                    if (packetnum == parameter.packetcount)
                    {
                        parameter.gradestatus = 0x01;
                        if (temp == 0x00)
                        {
                            Console.WriteLine("升级接收的指令正确 -- 8688.");
                        }
                        else if (temp == 0x01)
                        {
                            Console.WriteLine("升级结束.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("错误应答 -- 8688.");
                        parameter.gradestatus = 0x02;
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region port open and close
        public static List<string> GetCOMportList()
        {
            try
            {
                return System.IO.Ports.SerialPort.GetPortNames().ToList();
            }
            catch
            {
                return new List<string>();
            }
        }

        public string PortName { get; set; }
        public int Rate { get; set; }
        public int ReponseCommand { get; set; }
        public bool ResponseFlag = true;
        

        private System.IO.Ports.SerialPort InternalPortInstance { get; set; }
        private TcpClient tcpClient = new TcpClient();
        private NetworkStream sockStream = null;

        /*
            打开串口
            portRate：串口号和波特率（com0:115200）
            timeoutMS：
            status：输出状态（查找错误码表）
            示例：OpenSerial("COM15" + ":115200", 3000, out status);
       */
        public bool OpenSerial(string portRate, int timeoutMS, out ErrorCode status)
        {
            if (ChannelSelect == 0)
            {
                if (string.IsNullOrEmpty(portRate))
                {
                    status = ErrorCode.ParametterError;
                    return false;
                }

                //("COM16:115200")
                var paramArrary = portRate.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (paramArrary.Length != 2)
                {
                    status = ErrorCode.ParametterError;
                    return false;
                }

                PortName = paramArrary[0];
                int intRate;
                if (!int.TryParse(paramArrary[1], out intRate))
                {
                    status = ErrorCode.ParametterError;
                    return false;
                }
                Rate = intRate;

                status = ErrorCode.OK;

                System.Func<bool> openAction = OpenPort;

                var iAsyncResult = openAction.BeginInvoke(null, null);

                int waitingTimeMS = 0;

                //check if the open action finished or not every 5ms, if waiting time larger than timeoutMS, return false
                while (waitingTimeMS < timeoutMS)
                {
                    if (iAsyncResult.IsCompleted)
                    {
                        var result = openAction.EndInvoke(iAsyncResult);
                        status = result ? ErrorCode.OK : ErrorCode.SystemError;
                        return result;
                    }
                    else
                    {
                        Thread.Sleep(50);
                        waitingTimeMS += 50;
                    }
                }

                status = ErrorCode.Timeout;
                return false;
            }
            status = ErrorCode.SystemError;
            return false;
        }

        private bool OpenPort()
        {
            if (InternalPortInstance != null)
            {
                try
                {
                    if (InternalPortInstance.IsOpen)
                    {
                        InternalPortInstance.Close();
                    }
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                InternalPortInstance = new System.IO.Ports.SerialPort();
                InternalPortInstance.PortName = PortName;
                InternalPortInstance.BaudRate = Rate;

                InternalPortInstance.DataReceived += InternalPortInstance_DataReceived;
            }

            try
            {
                InternalPortInstance.Open();
            }
            catch
            {
                return false;
            }

            if (!InternalPortInstance.IsOpen)
            {
                return false;
            }
            else
            {
                ChannelSelect = 1;
                return true;
            }

        }

        /*
            关闭串口
            status：输出状态（查找错误码表）
       */
        public bool CloseSerial(out ErrorCode status)
        {
            ChannelSelect = 0;

            if (InternalPortInstance == null)
            {
                status = ErrorCode.OK;
                return true;
            }

            InternalPortInstance.DataReceived -= InternalPortInstance_DataReceived;

            try
            {
                InternalPortInstance.Close();
            }
            catch
            {
                status = ErrorCode.SystemError;
                return false;
            }

            if (InternalPortInstance.IsOpen)
            {
                status = ErrorCode.SystemError;
                return false;
            }

            InternalPortInstance = null;

            status = ErrorCode.OK;
            return true;
        }

        /*
            创建tcp客户端并连接服务器
            IpAddress：服务器ip地址（"192.168.0.163"）
            Port：服务器端口号（5050）
            status：输出状态（查找错误码表）
            示例：OpenEthernet("192.168.0.163", 5050, out status)
       */
        public bool OpenEthernet(string IpAddress, int Port, out ErrorCode status)
        {
            if (ChannelSelect == 0)
            {
                try
                {
                    tcpClient.Connect(IPAddress.Parse(IpAddress), Port);
                    sockStream = tcpClient.GetStream();
                    if(sockStream != null)
                    {
                        sockStream.BeginRead(TcpBuffer, 0, TcpBuffer.Length, new AsyncCallback(TcpClientReceiveCallback), sockStream);
                    }
                }
                catch(SocketException e)
                {
                    Console.WriteLine(e.Message);
                    if(sockStream != null)
                    {
                        sockStream.Close();
                    }
                    tcpClient.Close();
                    status = ErrorCode.SystemError;
                    return false;
                }

                ChannelSelect = 2;
                status = ErrorCode.OK;
                return true;
            }

            status = ErrorCode.SystemError;
            return false;
        }

        /*
            断开tcp连接，释放客户端资源
            status：输出状态（查找错误码表）
       */
        public bool CloseNetwork(out ErrorCode status)
        {
            ChannelSelect = 0;

            try
            {
                tcpClient.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            status = ErrorCode.OK;
            return true;
        }

        #endregion

        /*
            按照串口通信协议封装数据包
            cmd：命令字(查询命令字表)
            address：通信地址（0x00）
            data：数据段，参考串口协议（{ 0x01, 0x02, 0x03 }）
            示例：SendCommand(cmd_set_beep_mode, 0x00, { 0x01 })
       */
        public ErrorCode SendCommand(Commands cmd, byte address = 0x00, byte[] data = null)
        {
            var commandContent = new CommandContent();
            commandContent.Data = data == null ? new byte[] {  } : data;
            commandContent.Command = (byte)cmd;
            commandContent.Address = address;

            return WriteData(commandContent.GetCommandBytes());
        }

        public void InitResponseTimer()
        {
            if (!parameter.TimerInit)
            {
                Upgradetimer.Elapsed += new System.Timers.ElapsedEventHandler(TimeroutEvent);
                Upgradetimer.AutoReset = false;  // 不周期执行
                Upgradetimer.Enabled = true;     // 执行定时器事件
                parameter.TimerInit = true;
            }
        }

        /*
            读写器复位
            示例：SendCommand(cmd_set_beep_mode, 0x00, { 0x01 })
       */
        public ErrorCode MsgBaseReset()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_reset;

                commandContent.Command = (byte)Commands.cmd_reset;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置读写器串口波特率
            baudrate：波特率编号
            示例：MsgBaseSetUartBoudrate(0x09)
       */
        public ErrorCode MsgBaseSetUartBoudrate(byte baudrate)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_uart_baudrate;

                commandContent.Command = (byte)Commands.cmd_set_uart_baudrate;
                commandContent.Address = 0x00;

                commandContent.Data = new byte[1] { baudrate };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        //0x72 -- 获取读写器固件版本
        public ErrorCode MsgBaseGetFirmwareVer()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_firmware_version;

                commandContent.Command = (byte)Commands.cmd_get_firmware_version;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置读写器通信地址
            address：通信地址（0x00）
            示例：MsgBaseSetReaderAddress(0x01);
       */
        public ErrorCode MsgBaseSetReaderAddress(byte address)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_reader_address;

                commandContent.Command = (byte)Commands.cmd_set_reader_address;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[1] { address };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置读写器工作天线
            address：通信地址（0x00）
            示例：MsgBaseSetWorkAntenna(0x00); -- 天线1~天线8  -》 0x01~0x08
       */
        public ErrorCode MsgBaseSetWorkAntenna(byte antennaid)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_work_antenna;

                commandContent.Command = (byte)Commands.cmd_set_work_antenna;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[1] { antennaid };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            获取读写器工作天线
       */
        public ErrorCode MsgBaseGetWorkAntenna()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_work_antenna;

                commandContent.Command = (byte)Commands.cmd_get_work_antenna;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置读写器射频发射功率 -- 单天线
            rfpower：发射功率（0x21）
            示例：MsgBaseSetOutputPower(0x21);  --  0~33(0~0x21)dbm
       */
        public ErrorCode MsgBaseSetOutputPower(byte rfpower)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_output_power;

                commandContent.Command = (byte)Commands.cmd_set_output_power;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[1] { rfpower };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置读写器射频发射功率 -- 4天线
            ant1power：发射功率（0x21）
            ant2power：发射功率（0x21）
            ant3power：发射功率（0x21）
            ant4power：发射功率（0x21）
            示例：MsgBaseSet4AntPower(33,31,32,33);  --  0~33(0~0x21)dbm
       */
        public ErrorCode MsgBaseSet4AntPower(byte ant1power, byte ant2power, byte ant3power, byte ant4power)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_4_ant_power;

                commandContent.Command = (byte)Commands.cmd_set_4_ant_power;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[4] { ant1power, ant2power, ant3power, ant4power };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置读写器射频发射功率 -- 8天线
            ant1power：发射功率（0x21）
            ant2power：发射功率（0x21）
            ant3power：发射功率（0x21）
            ant4power：发射功率（0x21）
            ant5power：发射功率（0x21）
            ant6power：发射功率（0x21）
            ant7power：发射功率（0x21）
            ant8power：发射功率（0x21）
            示例：MsgBaseSet8AntPower(33,33,33,31,21,33,33,20);  --  0~33(0~0x21)dbm
       */
        public ErrorCode MsgBaseSet8AntPower(byte ant1power, byte ant2power, byte ant3power, byte ant4power, byte ant5power, byte ant6power, byte ant7power, byte ant8power)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_4_ant_power;

                commandContent.Command = (byte)Commands.cmd_set_4_ant_power;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[8] { ant1power, ant2power, ant3power, ant4power, ant5power, ant6power, ant7power, ant8power };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            获取读写器发射功率
       */
        public ErrorCode MsgBaseGetOutputPower()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_output_power;

                commandContent.Command = (byte)Commands.cmd_get_output_power;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置读写器工作频率范围
            region：标准（0x01:fcc）
            startFreq：起始频点（查看频点编号表）
            endFreq：结束频点（查看频点编号表）
            示例：MsgBaseSetFreqRegion(1, 0x07, 0x3b); -- 系统定义
       */
        public ErrorCode MsgBaseSetFreqRegion(byte region, byte startFreq, byte endFreq)
        {
            if ((region < 0x01) || (region > 0x03))
                return ErrorCode.RREC_UNSUPPORTED;

            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_frequency_region;

                commandContent.Command = (byte)Commands.cmd_set_frequency_region;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[3] { region, startFreq, endFreq };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置读写器工作频率范围
            FreqSpace：频点间隔（250kHz）
            FreqQuantity：频点个数
            StartFreq：起始频点（915000kHz）
            示例：MsgBaseSetFreqRegion(250, 2, 915000); -- 用户自定义
       */
        public ErrorCode MsgBaseSetFreqRegion(ushort FreqSpace, byte FreqQuantity, uint StartFreq)
        {
            byte index = 0;
            byte[] data = new byte[7];
            var commandContent = new CommandContent();

            if(FreqQuantity <= 0)
            {
                return ErrorCode.RREC_UNSUPPORTED;
            }

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_frequency_region;

                data[index++] = 0x04;
                data[index++] = (byte)(FreqSpace / 256);
                data[index++] = (byte)FreqSpace;
                data[index++] = FreqQuantity;
                data[index++] = (byte)(StartFreq / 256 / 256);
                data[index++] = (byte)(StartFreq / 256);
                data[index++] = (byte)StartFreq;

                commandContent.Command = (byte)Commands.cmd_set_frequency_region;
                commandContent.Address = 0x00;
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            获取读写器工作频率范围
       */
        public ErrorCode MsgBaseGetFreqRegion()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_frequency_region;

                commandContent.Command = (byte)Commands.cmd_get_frequency_region;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置蜂鸣器是否工作
            mode：0：不工作，1：工作
            示例：MsgBaseSetBeepMode(1);
       */
        public ErrorCode MsgBaseSetBeepMode(byte mode)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_beep_mode;

                commandContent.Command = (byte)Commands.cmd_set_beep_mode;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[1] { mode };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置读写器温度
            示例：MsgBaseGetReaderTemp();
       */
        public ErrorCode MsgBaseGetReaderTemp()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_reader_temperature;

                commandContent.Command = (byte)Commands.cmd_get_reader_temperature;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置蜂读写器临时功率
            rfpower：功率
            示例：MsgBaseSetTemporaryOutputPower(20-33);
       */
        public ErrorCode MsgBaseSetTemporaryOutputPower(byte rfpower)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_temporary_output_power;

                commandContent.Command = (byte)Commands.cmd_set_temporary_output_power;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[1] { rfpower };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置读写器带宽
            profile：带宽编号
            示例：MsgBaseSetRfLinkProfile(0xd0-0xd3);
       */
        public ErrorCode MsgBaseSetRfLinkProfile(byte profile)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_rf_link_profile;

                commandContent.Command = (byte)Commands.cmd_set_rf_link_profile;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[1] { profile };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            获取读写器带宽
            示例：MsgBaseGetRfLinkProfile();
       */
        public ErrorCode MsgBaseGetRfLinkProfile()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_rf_link_profile;

                commandContent.Command = (byte)Commands.cmd_get_rf_link_profile;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            检查天线是否连接
            示例：MsgBaseCheckAnt();
       */
        public ErrorCode MsgBaseCheckAnt()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_check_ant;

                commandContent.Command = (byte)Commands.cmd_check_ant;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            缓存盘存
            ant：工作天线
            示例：MsgBaseInventory();
       */
        public ErrorCode MsgBaseInventory(uint ant)
        {
            byte[] data = System.BitConverter.GetBytes(ant);
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_inventory;

                commandContent.Command = (byte)Commands.cmd_inventory;
                commandContent.Address = 0x00;
                //转换为大端模式
                Array.Reverse(data);
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            读
            membank：读取区域
            wordaddr：地址偏移
            wordcnt：读取字长
            password：访问地址
            示例：
       */
        public ErrorCode MsgBaseRead(byte membank, uint wordaddr, ushort wordcnt, uint password)
        {
            byte index = 0;
            byte[] data = new byte[11];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_read;

                commandContent.Command = (byte)Commands.cmd_read;
                commandContent.Address = 0x00;

                data[index++] = membank;
                data[index++] = (byte)(wordaddr / 256 / 256 / 256);
                data[index++] = (byte)(wordaddr / 256 / 256);
                data[index++] = (byte)(wordaddr / 256);
                data[index++] = (byte)wordaddr;
                data[index++] = (byte)(wordcnt / 256);
                data[index++] = (byte)wordcnt;
                data[index++] = (byte)(password / 256 / 256 / 256);
                data[index++] = (byte)(password / 256 / 256);
                data[index++] = (byte)(password / 256);
                data[index++] = (byte)password;
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            写
            password：访问地址
            membank：读取区域
            wordaddr：地址偏移
            wordcnt：读取字长
            Data：数据
            示例：
       */
        public ErrorCode MsgBaseWrite(uint password, byte membank, uint wordaddr, ushort wordcnt, byte[] Data)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_write;

                commandContent.Command = (byte)Commands.cmd_write;
                commandContent.Address = 0x00;

                data.Add((byte)(password / 256 / 256 / 256));
                data.Add((byte)(password / 256 / 256));
                data.Add((byte)(password / 256));
                data.Add((byte)password);
                data.Add(membank);
                data.Add((byte)(wordaddr / 256 / 256 / 256));
                data.Add((byte)(wordaddr / 256 / 256));
                data.Add((byte)(wordaddr / 256));
                data.Add((byte)wordaddr);
                data.Add((byte)(wordcnt / 256));
                data.Add((byte)wordcnt);
                data.AddRange(Data);

                commandContent.Data = data.ToArray();

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            锁定标签
            password：访问地址
            membank：读取区域
            locktype：锁定类型
            示例：
       */
        public ErrorCode MsgBaseLock(uint password, byte membank, byte locktype)
        {
            byte index = 0;
            byte[] data = new byte[6];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_lock;

                commandContent.Command = (byte)Commands.cmd_lock;
                commandContent.Address = 0x00;

                data[index++] = (byte)(password / 256 / 256 / 256);
                data[index++] = (byte)(password / 256 / 256);
                data[index++] = (byte)(password / 256);
                data[index++] = (byte)password;
                data[index++] = membank;
                data[index++] = locktype;

                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            灭活标签
            password：访问地址
            示例：
       */
        public ErrorCode MsgBaseKill(uint password)
        {
            byte index = 0;
            byte[] data = new byte[4];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_kill;

                commandContent.Command = (byte)Commands.cmd_kill;
                commandContent.Address = 0x00;

                data[index++] = (byte)(password / 256 / 256 / 256);
                data[index++] = (byte)(password / 256 / 256);
                data[index++] = (byte)(password / 256);
                data[index++] = (byte)password;

                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置匹配epc号
            mode：0：有效，1：清除匹配
            epclen：epc长度
            epc：epc号
            示例：
       */
        public ErrorCode MsgBaseSetAccessEpcMatch(byte mode, byte epclen, byte[] epc)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_access_epc_match;

                commandContent.Command = (byte)Commands.cmd_set_access_epc_match;
                commandContent.Address = 0x00;

                data.Add(mode);
                data.Add(epclen);
                data.AddRange(epc);

                commandContent.Data = data.ToArray();

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            获取匹配的epc号
            示例：
       */
        public ErrorCode MsgBaseGetAccessEpcMatch()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_access_epc_match;

                commandContent.Command = (byte)Commands.cmd_get_access_epc_match;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            多天线轮询盘存
        */
        public ErrorCode MsgBaseFastSwitchAntInventory(byte ant_num, byte[] ant_data, byte interval, byte session, byte flag, byte repeat)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_fast_switch_ant_inventory;

                commandContent.Command = (byte)Commands.cmd_fast_switch_ant_inventory;
                commandContent.Address = 0x00;

                data.Add(ant_num);
                data.AddRange(ant_data);
                data.Add(interval);
                data.Add(session);
                data.Add(flag);
                data.Add(repeat);

                commandContent.Data = data.ToArray();

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x89
            开始实时盘存
        */
        public ErrorCode MsgStartInventory(byte antName)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_real_time_inventory;

                commandContent.Command = (byte)Commands.cmd_real_time_inventory;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[1] { antName };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x8a
            用户自定义盘存
        */
        public ErrorCode MsgBaseCastomInventory(byte ant, byte inven_num, byte match_mode, byte match_num)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_custom_inventory;

                commandContent.Command = (byte)Commands.cmd_custom_inventory;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[4] { ant, inven_num, match_mode, match_num };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x8b
            用户自定义盘存
        */
        public ErrorCode MsgBaseCustmizedSessionTargetInventoty(byte session, byte target)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_customized_session_target_invent;

                commandContent.Command = (byte)Commands.cmd_customized_session_target_invent;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[2] { session, target };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x8c
            停止盘存
        */
        public ErrorCode MsgBaseStopInventory()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_stop_inventory;

                commandContent.Command = (byte)Commands.cmd_stop_inventory;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x90
            获取缓存盘存到的标签
        */
        public ErrorCode MsgBaseGetInventoryBuffer()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_inventory_buffer;

                commandContent.Command = (byte)Commands.cmd_get_inventory_buffer;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x91
            获取并清空缓存盘存
        */
        public ErrorCode MsgBaseGetAndResetInventoryBuffer()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_and_reset_inventory_buffer;

                commandContent.Command = (byte)Commands.cmd_get_and_reset_inventory_buffer;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x92
            获取缓存中的标签数量
        */
        public ErrorCode MsgBaseInventoryBufferTagCount()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_inventory_buffer_tag_count;

                commandContent.Command = (byte)Commands.cmd_get_inventory_buffer_tag_count;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x93
            清空缓存
        */
        public ErrorCode MsgBaseResetInventoryBuffer()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_reset_inventory_buffer;

                commandContent.Command = (byte)Commands.cmd_reset_inventory_buffer;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x95
            sm7加密写
        */
        public ErrorCode MsgBaseSM7Write(uint password, ushort wordcnt, byte[] Data)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_sm7_write;

                commandContent.Command = (byte)Commands.cmd_sm7_write;
                commandContent.Address = 0x00;

                data.Add((byte)(password / 256 / 256 / 256));
                data.Add((byte)(password / 256 / 256));
                data.Add((byte)(password / 256));
                data.Add((byte)password);
                data.Add((byte)(wordcnt / 256));
                data.Add((byte)wordcnt);
                data.AddRange(Data);

                commandContent.Data = data.ToArray();

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x96
            sm7解密读
        */
        public ErrorCode MsgBaseSM7Read(byte read_len)
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_sm7_read;

                commandContent.Command = (byte)Commands.cmd_sm7_read;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[1] { read_len };

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x97
            更新sm7秘钥
        */
        public ErrorCode MsgBaseSM7PKUpdate(byte[] password)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_sm7_pk_update;

                commandContent.Command = (byte)Commands.cmd_sm7_pk_update;
                commandContent.Address = 0x00;

                data.AddRange(password);

                commandContent.Data = data.ToArray();

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x42
            复旦微国标加密通信写
        */
        public ErrorCode MsgBaseSRCWrite(uint password, byte membank, ushort wordaddr, ushort wordcnt, byte[] Data)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_gb_seu_write;

                commandContent.Command = (byte)Commands.cmd_gb_seu_write;
                commandContent.Address = 0x00;

                data.Add((byte)(password / 256 / 256 / 256));
                data.Add((byte)(password / 256 / 256));
                data.Add((byte)(password / 256));
                data.Add((byte)password);
                data.Add(membank);
                data.Add((byte)(wordaddr / 256));
                data.Add((byte)wordaddr);
                data.Add((byte)(wordcnt / 256));
                data.Add((byte)wordcnt);
                data.AddRange(Data);

                commandContent.Data = data.ToArray();

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            cmd = 0x43
            复旦微国标加密读
        */
        public ErrorCode MsgBaseSECRead(byte membank, ushort wordaddr, ushort wordcnt, uint password)
        {
            byte index = 0;
            byte[] data = new byte[9];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_gb_seu_read;

                commandContent.Command = (byte)Commands.cmd_gb_seu_read;
                commandContent.Address = 0x00;

                data[index++] = membank;
                data[index++] = (byte)(wordaddr / 256);
                data[index++] = (byte)wordaddr;
                data[index++] = (byte)(wordcnt / 256);
                data[index++] = (byte)wordcnt;
                data[index++] = (byte)(password / 256 / 256 / 256);
                data[index++] = (byte)(password / 256 / 256);
                data[index++] = (byte)(password / 256);
                data[index++] = (byte)password;
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        //0x44
       
        public ErrorCode MsgBaseGBRealTimeInventory()
        {
            return ErrorCode.RREC_FAIL;
        }

        /*
           cmd = 0x45
           GB读
       */
        public ErrorCode MsgBaseGBRead(byte membank, ushort wordaddr, ushort wordcnt, uint password)
        {
            byte index = 0;
            byte[] data = new byte[9];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_gb_read;

                commandContent.Command = (byte)Commands.cmd_gb_read;
                commandContent.Address = 0x00;

                data[index++] = membank;
                data[index++] = (byte)(wordaddr / 256);
                data[index++] = (byte)wordaddr;
                data[index++] = (byte)(wordcnt / 256);
                data[index++] = (byte)wordcnt;
                data[index++] = (byte)(password / 256 / 256 / 256);
                data[index++] = (byte)(password / 256 / 256);
                data[index++] = (byte)(password / 256);
                data[index++] = (byte)password;
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
           cmd = 0x46
           GB写
       */
        public ErrorCode MsgBaseGBWrite(uint password, byte membank, ushort wordaddr, ushort wordcnt, byte[] Data)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_gb_write;

                commandContent.Command = (byte)Commands.cmd_gb_write;
                commandContent.Address = 0x00;

                data.Add((byte)(password / 256 / 256 / 256));
                data.Add((byte)(password / 256 / 256));
                data.Add((byte)(password / 256));
                data.Add((byte)password);
                data.Add(membank);
                data.Add((byte)(wordaddr / 256));
                data.Add((byte)wordaddr);
                data.Add((byte)(wordcnt / 256));
                data.Add((byte)wordcnt);
                data.AddRange(Data);

                commandContent.Data = data.ToArray();

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
           cmd = 0x47
           GB锁定
       */
        public ErrorCode MsgBaseGBLock(uint password, byte membank, byte locktype)
        {
            byte index = 0;
            byte[] data = new byte[6];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_gb_lock;

                commandContent.Command = (byte)Commands.cmd_gb_lock;
                commandContent.Address = 0x00;

                data[index++] = (byte)(password / 256 / 256 / 256);
                data[index++] = (byte)(password / 256 / 256);
                data[index++] = (byte)(password / 256);
                data[index++] = (byte)password;
                data[index++] = membank;
                data[index++] = locktype;

                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
           cmd = 0x49
           GB灭活
       */
        public ErrorCode MsgBaseGBKill(uint password)
        {
            byte index = 0;
            byte[] data = new byte[6];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_gb_kill;

                commandContent.Command = (byte)Commands.cmd_gb_kill;
                commandContent.Address = 0x00;

                data[index++] = (byte)(password / 256 / 256 / 256);
                data[index++] = (byte)(password / 256 / 256);
                data[index++] = (byte)(password / 256);
                data[index++] = (byte)password;

                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
           cmd = 0x98
           复旦微GB双向认证
       */
        public ErrorCode MsgBaseGBMulSeuAuth(byte[] password)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_gb_mul_seu_auth;

                commandContent.Command = (byte)Commands.cmd_gb_mul_seu_auth;
                commandContent.Address = 0x00;

                data.AddRange(password);

                commandContent.Data = data.ToArray();

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
           cmd = 0x4a
           保存参数
       */
        public ErrorCode MsgBaseReaderParaSave()
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_reader_para_save;

                commandContent.Command = (byte)Commands.cmd_reader_para_save;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
           cmd = 0x4b
           恢复出厂设置
       */
        public ErrorCode MsgBaseReaderParaReset()
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_reader_para_reset;

                commandContent.Command = (byte)Commands.cmd_reader_para_reset;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        //0x4c
        public ErrorCode MsgBaseReaderAppUpgrade(uint packetserialnumber, byte status, byte[] Data)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            commandContent.Command = (byte)Commands.cmd_reader_app_upgrade;
            commandContent.Address = 0x00;

            data.Add((byte)(packetserialnumber / 256 / 256 / 256));
            data.Add((byte)(packetserialnumber / 256 / 256));
            data.Add((byte)(packetserialnumber / 256));
            data.Add((byte)packetserialnumber);

            data.Add(status);
            data.Add((byte)(Data.Length));
            data.AddRange(Data);

            commandContent.Data = data.ToArray();

            return WriteData(commandContent.GetCommandBytes());
        }

        //0x4d
        public ErrorCode MsgBasebandFirmwareUpgrade(uint packetserialnumber, byte status, byte[] Data)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            commandContent.Command = (byte)Commands.cmd_baseband_firmware_upgrade;
            commandContent.Address = 0x00;

            data.Add((byte)(packetserialnumber / 256 / 256 / 256));
            data.Add((byte)(packetserialnumber / 256 / 256));
            data.Add((byte)(packetserialnumber / 256));
            data.Add((byte)packetserialnumber);

            data.Add(status);
            data.Add((byte)(Data.Length));
            data.AddRange(Data);

            commandContent.Data = data.ToArray();

            return WriteData(commandContent.GetCommandBytes());
        }

        //应用升级接口
        public ErrorCode MsgBaseAppUpgrage(string filepath, GetUpgrageCallback UpgrageCallback)
        {
            int sendlength = 0;
            long dataindex = 0;
            long filelength = 0;
            byte[] filedata = new byte[parameter.AppUpgradeDataLen];
            byte upgradecount = 0;
            byte status = 0x00;

            parameter.packetcount = 0;
            parameter.GradeResult = 0x00;

            if (!File.Exists(filepath))
            {
                Console.WriteLine("文件路径不存在.");
                return ErrorCode.ParametterError;
            }

            if (!parameter.TimerInit)
            {
                Upgradetimer.Elapsed += new System.Timers.ElapsedEventHandler(TimeroutEvent);
                Upgradetimer.AutoReset = false;  // 不周期执行
                Upgradetimer.Enabled = true;     // 执行定时器事件
                parameter.TimerInit = true;
            }
            using (FileStream fs = File.OpenRead(filepath))
            {
                // 开启定时器
                Upgradetimer.Start();

                //获取文件长度
                filelength = fs.Length;//209320byte
                Console.WriteLine("file length:" + filelength);

                while (filelength > dataindex)
                {
                    sendlength = ((int)(filelength - dataindex) > parameter.AppUpgradeDataLen) ? parameter.AppUpgradeDataLen : (int)(filelength - dataindex);
                    if(filelength > (dataindex + sendlength))
                    {
                        status = 0x00;
                    }
                    else
                    {
                        //最后一个包
                        status = 0x01;
                    }

                    Console.WriteLine("len:" + sendlength);
                    Console.WriteLine("count:" + parameter.packetcount);
                    fs.Read(filedata, 0, sendlength);

                    byte[] senddata = filedata.Skip(0).Take(sendlength).ToArray();

                    do
                    {
                        parameter.gradestatus = 0x00;
                        MsgBaseReaderAppUpgrade(parameter.packetcount, status, senddata);
                        while (parameter.gradestatus == 0x00)                                //等待升级的结果
                        {
                            if (parameter.GradeResult == 0x01)
                            {
                                Upgradetimer.Stop();
                                return ErrorCode.ParametterError;
                            }
                        }
                        upgradecount += 1;
                        if(upgradecount >= 3)
                        {
                            Upgradetimer.Stop();
                            return ErrorCode.ParametterError;
                        }

                    }
                    while (parameter.gradestatus == 0x02);                                  //升级失败，重复执行升级命令

                    upgradecount = 0;
                    parameter.gradestatus = 0x00;                                           //更新升级状态
                    parameter.packetcount += 1;                                             //更新升级的包
                    dataindex += sendlength;
                    Console.WriteLine("当前包升级成功。");

                    UpgrageCallback(filelength, dataindex);

                    // 重启定时器
                    Upgradetimer.Stop();
                    Upgradetimer.Start();
                }
            }

            // 关闭定时器
            Upgradetimer.Stop();

            Console.WriteLine("升级结束...\r\n");
            //升级结束
            parameter.gradestatus = 0x00;
            parameter.packetcount = 0;

            return ErrorCode.OK;
        }

        //应用升级接口
        public ErrorCode MsgBasebandUpgrade(string filepath, GetUpgrageCallback UpgrageCallback)
        {
            int sendlength = 0;
            long dataindex = 0;
            long filelength = 0;
            byte[] filedata = new byte[parameter.AppUpgradeDataLen];
            byte upgradecount = 0;
            byte status = 0x00;
            byte timeoutflag = 0;

            if (parameter.GradeResult == 0x01)
            {
                Console.WriteLine("下载超时，重新下载。");
                timeoutflag = 1;
            }

                parameter.packetcount = 0;
            parameter.GradeResult = 0x00;

            if (!File.Exists(filepath))
            {
                Console.WriteLine("文件路径不存在.");
                return ErrorCode.ParametterError;
            }

            if (!parameter.TimerInit)
            {
                Upgradetimer.Elapsed += new System.Timers.ElapsedEventHandler(TimeroutEvent);
                Upgradetimer.AutoReset = false;  // 不周期执行
                Upgradetimer.Enabled = true;     // 执行定时器事件
                parameter.TimerInit = true;
            }
            using (FileStream fs = File.OpenRead(filepath))
            {
                // 开启定时器
                Upgradetimer.Start();

                //获取文件长度
                filelength = fs.Length;//byte
                Console.WriteLine("file length:" + filelength);

                while (filelength > dataindex)
                {
                    sendlength = ((int)(filelength - dataindex) > parameter.AppUpgradeDataLen) ? parameter.AppUpgradeDataLen : (int)(filelength - dataindex);
                    if (filelength > (dataindex + sendlength))
                    {
                        status = 0x00;
                    }
                    else
                    {
                        //最后一个包
                        status = 0x01;
                    }

                    Console.WriteLine("len:" + sendlength);
                    Console.WriteLine("count:" + parameter.packetcount);
                    fs.Read(filedata, 0, sendlength);
                    if(timeoutflag == 1)
                    {
                        Console.WriteLine("len:" + sendlength);
                        Console.WriteLine("count:" + parameter.packetcount);
                        fs.Read(filedata, 0, sendlength);
                        timeoutflag = 0;
                    }

                    byte[] senddata = filedata.Skip(0).Take(sendlength).ToArray();

                    do
                    {
                        parameter.gradestatus = 0x00;
                        MsgBasebandFirmwareUpgrade(parameter.packetcount, status, senddata);
                        while (parameter.gradestatus == 0x00)                                //等待升级的结果
                        {
                            if(parameter.GradeResult == 0x01)
                            {
                                Upgradetimer.Stop();
                                return ErrorCode.ParametterError;
                            }
                        }
                        upgradecount += 1;
                        if (upgradecount >= 3)
                        {
                            Upgradetimer.Stop();
                            return ErrorCode.ParametterError;
                        }

                    }
                    while (parameter.gradestatus == 0x02);                                  //升级失败，重复执行升级命令

                    upgradecount = 0;
                    parameter.gradestatus = 0x00;                                           //更新升级状态
                    parameter.packetcount += 1;                                             //更新升级的包
                    dataindex += sendlength;
                    Console.WriteLine("当前包升级成功。");

                    UpgrageCallback(filelength, dataindex);

                    // 重启定时器
                    Upgradetimer.Stop();
                    Upgradetimer.Start();
                }
            }

            // 关闭定时器
            Upgradetimer.Stop();

            Console.WriteLine("升级结束...\r\n");
            //升级结束
            parameter.gradestatus = 0x00;
            parameter.packetcount = 0;

            return ErrorCode.OK;
        }

        /*
            获取GPIO状态
        */
        public ErrorCode MsgBaseGetGPIOSTatus()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_gpio_status;

                commandContent.Command = (byte)Commands.cmd_get_gpio_status;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置GPIO状态
       */
        public ErrorCode MsgBaseSetGPIOSTatus(byte gpioName, byte status)
        {
            byte index = 0;
            byte[] data = new byte[2];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_gpio_status;

                data[index++] = gpioName;
                data[index++] = status;

                commandContent.Command = (byte)Commands.cmd_set_gpio_status;
                commandContent.Address = 0x00;
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            获取多标签状态
        */
        public ErrorCode MsgBaseMulTagModeStatus()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_mulTag_mode_status;

                commandContent.Command = (byte)Commands.cmd_get_mulTag_mode_status;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            设置多标签状态
       */
        public ErrorCode MsgBaseSetMulTagModeStatus(byte status)
        {
            byte index = 0;
            byte[] data = new byte[7];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_mulTag_mode_status;

                data[index++] = status;

                commandContent.Command = (byte)Commands.cmd_set_mulTag_mode_status;
                commandContent.Address = 0x00;
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        //双向认证
        public ErrorCode MsgBaseBidirectionalAuthentication(uint packetserialnumber, byte status, byte[] Data)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            commandContent.Command = (byte)Commands.cmd_bidirectional_authentication;
            commandContent.Address = 0x00;

            data.Add((byte)(Data.Length));
            data.AddRange(Data);

            commandContent.Data = data.ToArray();

            return WriteData(commandContent.GetCommandBytes());
        }

        //设置tx占空比
        public ErrorCode MsgBaseSetTxTime(ushort txOnTime, ushort txOffTime)
        {
            byte index = 0;
            byte[] data = new byte[4];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_tx_time;

                commandContent.Command = (byte)Commands.cmd_set_tx_time;
                commandContent.Address = 0x00;

                data[index++] = (byte)(txOnTime / 256);
                data[index++] = (byte)txOnTime;
                data[index++] = (byte)(txOffTime / 256);
                data[index++] = (byte)txOffTime;
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
            获取tx占空比
        */
        public ErrorCode MsgBaseGetTxTime()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_tx_time;

                commandContent.Command = (byte)Commands.cmd_get_tx_time;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        //设置session和target
        public ErrorCode MsgBaseSetSessionTarget(byte session, byte target)
        {
            byte index = 0;
            byte[] data = new byte[2];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_session_target;

                commandContent.Command = (byte)Commands.cmd_set_session_target;
                commandContent.Address = 0x00;

                data[index++] = session;
                data[index++] = target;
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
           获取session和target
        */
        public ErrorCode MsgBaseGetSessionTarget()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_session_target;

                commandContent.Command = (byte)Commands.cmd_get_session_target;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        //设置CW波
        public ErrorCode MsgBaseSetCW(byte cwSwitch)
        {
            byte index = 0;
            byte[] data = new byte[1];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_cw;

                commandContent.Command = (byte)Commands.cmd_set_cw;
                commandContent.Address = 0x00;

                data[index++] = cwSwitch;
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
           获取CW波
        */
        public ErrorCode MsgBaseGetCW()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_cw;

                commandContent.Command = (byte)Commands.cmd_get_cw;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        //设置心跳和上报周期
        public ErrorCode MsgBaseSetKeepalive(byte enable, short reportTime)
        {
            byte index = 0;
            byte[] data = new byte[3];
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_set_keepalive;

                commandContent.Command = (byte)Commands.cmd_set_keepalive;
                commandContent.Address = 0x00;

                data[index++] = enable;
                data[index++] = (byte)(reportTime / 256);
                data[index++] = (byte)reportTime;
                commandContent.Data = data;

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        /*
           获取设置心跳和上报周期
        */
        public ErrorCode MsgBaseGetKeepalive()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_keepalive;

                commandContent.Command = (byte)Commands.cmd_get_keepalive;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }

        //获取select
        public ErrorCode MsgBaseSetSelect(byte enable, byte selParam, int pointer, byte maskLen, byte trucation, byte[] Data)
        {
            List<byte> data = new List<byte>();
            var commandContent = new CommandContent();

            commandContent.Command = (byte)Commands.cmd_set_select;
            commandContent.Address = 0x00;

            data.Add(enable);
            data.Add(selParam);
            data.Add((byte)(pointer / 256 / 256 / 256));
            data.Add((byte)(pointer / 256 / 256));
            data.Add((byte)(pointer / 256));
            data.Add((byte)pointer);
            data.Add(maskLen);
            data.Add(trucation);
            data.AddRange(Data);

            commandContent.Data = data.ToArray();

            return WriteData(commandContent.GetCommandBytes());
        }

        /*
           获取select
        */
        public ErrorCode MsgBaseGetSelect()
        {
            var commandContent = new CommandContent();

            InitResponseTimer();

            if (ResponseFlag)
            {
                ResponseFlag = false;
                Upgradetimer.Start();
                ReponseCommand = (int)Commands.cmd_get_select;

                commandContent.Command = (byte)Commands.cmd_get_select;
                commandContent.Address = 0x00;
                commandContent.Data = new byte[0];

                return WriteData(commandContent.GetCommandBytes());
            }
            else
            {
                Console.WriteLine("发送失败.");
                return ErrorCode.RREC_FAIL;
            }
        }








        private void TimeroutEvent(object sender, EventArgs e)
        {
            // 关闭定时器
            Upgradetimer.Stop();

            // 超时开启下一次发送使能
            ResponseFlag = true;

            // 下载失败
            parameter.GradeResult = 0x01;

            Console.WriteLine("接收应答数据超时...\r\n");
        }

        private ErrorCode WriteData(byte[] content)
        {
            WriteLog("WriteData:" + ToHexString(content));

            try
            {
                if (ChannelSelect == 1)
                {
                    InternalPortInstance.Write(content, 0, content.Length);
                }
                else if(ChannelSelect == 2)
                {
                    if (sockStream != null)
                    {
                        sockStream.Write(content, 0, content.Length);
                    }
                    else
                    {
                        Console.WriteLine("TCP发送数据出错...\r\n");
                    }
                }
                else
                {
                    Console.WriteLine("发送数据出错...\r\n");
                }

            }
            catch (InvalidOperationException e)
            {
                WriteLog("WriteData: Exception " + e.Message);
            }
            catch (Exception e)
            {
                WriteLog("WriteData: Unknown Exception " + e.Message);
            }

            return ErrorCode.OK;
        }

        public void Dispose()
        {
            if (InternalPortInstance != null)
            {
                var status = ErrorCode.OK;
                CloseSerial(out status);
            }
        }

        private Queue<byte> recevicedBufferCache = new Queue<byte>();
        private static byte[] TcpBuffer = new byte[1024];
        private int currentExpectedLength = 0;
        private List<byte> currentResponce = new List<byte>();
        private static int ChannelSelect = 0;                                                      // 数据传输通道，0-未打开任何通道，1-串口，2-网口
        
        private void InternalPortInstance_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int receivedByteLength = InternalPortInstance.BytesToRead;
            byte[] buffer = new byte[receivedByteLength];
            InternalPortInstance.Read(buffer, 0, receivedByteLength);

            lock (recevicedBufferCache)
            {
                for (int i = 0; i < receivedByteLength; i++)
                {
                    recevicedBufferCache.Enqueue(buffer[i]);
                }
            }

            SpliteQueueData();
        }

        private void TcpClientReceiveCallback(IAsyncResult result)
        {
            int BufferLengh = 0;
            var status = ErrorCode.OK;
            bool connectStatus = tcpClient.Connected;

            if (connectStatus)
            {
                NetworkStream SockStream = (NetworkStream)result.AsyncState;
                BufferLengh = SockStream.EndRead(result);
                result.AsyncWaitHandle.Close();

                if (BufferLengh == 0)
                {
                    Console.WriteLine("断开连接。\r\n");
                    CloseNetwork(out status);
                }

                Console.WriteLine(TcpBuffer);
                lock (recevicedBufferCache)
                {
                    for (int i = 0; i < BufferLengh; i++)
                    {
                        recevicedBufferCache.Enqueue(TcpBuffer[i]);
                    }
                }

                //清空数据，重新开始异步接收
                TcpBuffer = new byte[TcpBuffer.Length];
                SockStream.BeginRead(TcpBuffer, 0, TcpBuffer.Length, new AsyncCallback(TcpClientReceiveCallback), SockStream);

                SpliteQueueData();
            }
            else
            {
                result.AsyncWaitHandle.Close();
                CloseNetwork(out status);
                Console.WriteLine("接收回调错误。\n");
            }
        }

        private void SpliteQueueData()
        {
            if (recevicedBufferCache.Count == 0)
                return;

            lock (recevicedBufferCache)
            {
                while (recevicedBufferCache.Count > 0)
                {

                    var currentByte = recevicedBufferCache.Dequeue();

                    //这是一个新的返回头
                    if (currentResponce.Count == 0)
                    {
                        if (currentByte == CommandContent.Head)
                        {
                            currentResponce.Add(currentByte);
                            continue;
                        }
                    }

                    //仅接收到0xA0头,那么下一个应该是length
                    if (currentResponce.Count == 1)
                    {
                        currentResponce.Add(currentByte);
                        currentExpectedLength = currentByte;

                        continue;
                    }

                    //已接受到0xA0头和lengh
                    if (currentResponce.Count >= 2)
                    {
                        currentResponce.Add(currentByte);
                        currentExpectedLength -= 1;

                        //如果这时期待的字节为0，那么就是接收完全了一个返回结果
                        if (currentExpectedLength == 0)
                        {
                            var cmdResponce = currentResponce.ToArray();
                            WriteLog("Receive:" + ToHexString(cmdResponce));
                            SendOutCommandResponce(cmdResponce);

                            currentResponce = new List<byte>();
                            currentExpectedLength = 0;

                        }

                        continue;
                    }
                }
            }
        }

        #region byte<->string

        /// <summary>
        /// byte[]转16进制string
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ToHexString(byte[] bytes)
        {
            string hexString = string.Empty;
            if (bytes != null)
            {
                StringBuilder strB = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    strB.Append(' ' + bytes[i].ToString("X2"));
                }
                hexString = strB.ToString();
            }
            return hexString;
        }
        /// <summary>
        /// 16进制字符串转byte[]
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static byte[] GetBytesFromString(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
            {
                hexString += " ";
            }
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
            {
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }
            return returnBytes;
        }

        #endregion
    }

    public class parameter
    {
        public static int AppUpgradeDataLen = 200;                   //升级包data段的长度
        public static ErrorCode errorCode = ErrorCode.OK;
        public static uint packetcount = 0;                         //升级中发送包的计数
        public static byte gradestatus = 0x00;                      //升级过程中指示当前状态 0x00-等待应答，0x01-升级成功，0x02-升级失败
        public static int GradeResult = 0x00;                       // 升级结果
        public static bool TimerInit = false;                       // 是否初始化了定时器
    }
}
